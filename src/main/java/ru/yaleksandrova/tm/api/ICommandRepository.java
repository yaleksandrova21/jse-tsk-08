package ru.yaleksandrova.tm.api;

import ru.yaleksandrova.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
